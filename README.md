# [Expanding Man's Reviews](https://expandingman.gitlab.io/reviews)
A collection of reviews.  There are reviews written by me and references to helpful reviews written
by others.


## Building
Run the script `build.sh`.  This requires the [`engrafo`](https://github.com/arxiv-vanity/engrafo)
(for rendering the reviews) docker image to be available and [`quarto`](https://quarto.org/) (for building the website)
to be available.

Building should be done before committing.  The CI/CD process does *NOT* build.
