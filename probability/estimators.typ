#import "lib.typ": *

= Estimators and Hypothesis Testing
We cannot observe probability distributions directly, instead they must be inferred form statistical
properties of samples.  For example, if by hypothesis we observe the outcome of a process that's
distributed according to $cal(N)(mu, sigma)$, we cannot determine $mu$ or $sigma$ exactly, but
instead must estimate them from the properties of a finite sample.  A statistic that we use to
estimate a distribution parameter is called an *estimator*.  These are of great practical
importance, so we will discuss them in more detail in this section.

== Expected Error
As estimators are computed from finite samples, they will themselves have statistical properties
that must be understood for them to be useful.  An estimator can be expressed as some function of
random variables $hat(theta)(X_1,...,X_n)$.  Estimators are frequently denoted with a $hat$, a
convention we will adhere to here.  We may hide the $X_j$ dependence for convenience where there is
no risk of confusion.  We'd like to quantify the difference between our estimator $hat(theta)$ and
some parameter of the hypothesized distribution $theta$, so it makes sense to consider the *mean
quared error* (MSE) defined as $expect((hat(theta) - theta)^2)$ (note that this is _only_ the same
thing as the variance of $theta$ is indeed the mean of $hat(theta)$, a topic we will broach
momentarily).  Note that
$ "MSE"[hat(theta)] := expect((hat(theta) - theta)^2) =
  expect(hat(theta)^2) - 2 theta expect(hat(theta)) + theta^2
$
but by splitting the first term on the right hand side using $var[hat(theta)] = expect(hat(theta)^2)
- expect(hat(theta))^2$ and re-combining terms we find
$ "MSE"[hat(theta)] = (expect(hat(theta)) - theta)^2 + var[hat(theta)] $<bias-var-tradeoff>
The square root of the first term $expect(hat(theta)) - theta$ is called the *bias*, and is simply
the difference between the mena of the estimator and the parameter it is supposed to estimate.
@bias-var-tradeoff shows an important fundamental limitation of all statistical estimators.  Both
the bias and the variance contribute to the expected error.  Usually we seek an estimator which
minimizes the MSE.  In many cases, we may use an *unbiased* estimator, meaning an estimator for
which $expect(hat(theta)) = theta$, however, in practice this often comes at the expense of
$var[hat(theta)]$.  This is an important concept in statistical learning known as the *bias-variance
tradeoff*.  As we will discuss in more detail in a future section in empirical risk minimization,
typically more "finely detailed" models with a larger number of parameters have a small bias but
large variance, whereas simpler models with fewer degrees of freedom have smaller variance but in
some cases may have irreducible bias.

== Sample Mean
As we have discussed, the CLT is suggestive of a way to estimate the mean $expect(X)$.  Since the
distribution of $sum_j X_j$ for i.i.d. random variables approaches $cal(N)(mu, sigma/sqrt(n))$, we
can take as an estimator of the sample mean $(X_1 + dots.c + X_n) slash n$.  That is, given a series
of random variables $X_1,...,X_n$, we take as our sample mean estimator
$ hat(mu) = 1/n sum_(j=1)^n X_j $
and the CLT guarantees that $expect(hat(mu)) = mu$ where $mu$ is the true mean of the population
distribution.  If the population distribution is Gaussian, also by the CLT we have $var[hat(mu)] =
sigma^2/n$, otherwise this will be approximately true for large $n$.

Note that in the above it may appear that we conflated random variables $X_j$ with "observation
measurements".  We can do this becuase the ensemble of all sequences of measurements is the same as
the statistical ensemble of the random variables $X_1,...,X_n$, under the assumption that
observations are i.i.d.  In other words, we should express estimators as a function of random
variables, a particular sequence of measurements corresponds to a particular realization of the
sequence of random variables.

== Sample Variance
From the definition of variance, we might assume a good estimator might be
$ tilde(sigma)^2 = 1/n sum_(j=1)^n (X_j - hat(mu))^2 $
(the use of a $tilde$ rather than a $hat$ here is deliberate).  The mean of this estimator is
$ expect(tilde(sigma)^2) &=
  1/n sum_(j=1)^n (expect(X_j^2) - 2 expect(X_j hat(mu)) + expect(hat(mu)^2)) \
  &= 1/n sum_(j=1)^n (mu^2 + sigma^2 - 2 expect(X_j hat(mu)) + mu^2 + sigma^2/n) \
  &= 2 mu^2 + (n+1)/n sigma^2 - 2 expect(X hat(mu))
$<sample-var-1>
where $mu = expect(X)$ and $sigma^2 = var[X]$.  Here we use $X$ to mean any $X_j$ since they are
identically distributed.  We have also repeatedly used $expect(X^2) = mu^2 + sigma^2$.  Now we
need
$ expect(X_j hat(mu)) = 1/n sum_(k=1)^n expect(X_j X_k) &=
  1/n (expect(X^2) + sum_(k != j) expect(X_j X_k)) \
  &= 1/n (mu^2 + sigma^2 + (n - 1) mu^2) \
  &= mu^2 + sigma^2/n
$
Combining this with @sample-var-1 we find
$ expect(tilde(sigma)^2) = 2 mu^2 + (n+1)/n sigma^2 - 2 mu^2 - (2 sigma^2)/n = (n-1)/n sigma^2 $
From this we see that $tilde(sigma)^2$ is biased in that its mean differs from $sigma^2$ by a factor
of $(n - 1) slash n$.  Knowing this, it is trivial to construct the unbiased estimator
$ hat(sigma)^2 = 1/(n-1) sum_(j = 1)^n (X_j - hat(mu))^2 $
This result is known as the scourge of statistics undergraduates everywhere.  Fortunately, for large
$n$ the bias of $tilde(sigma)^2$ becomes negligible, so the discrepancy between this and the
unbiased estimator $hat(sigma)^2$ tends not to be relevant in applications with appreciable sample
sizes. Note that $hat(mu)$, not $mu$ appears in our expression for $hat(sigma)^2$, since we can
oberve $hat(mu)$ but not $mu$.

As one should expect, $var[hat(sigma)^2] prop sigma^4$.  Deriving the estimator variance is tedious,
but straightforward, so we merely state it here without proof
$ var[hat(sigma)^2] = sigma^4/n (m_4 - 1 + 2/(n-1)) $
where $m_4$ is the fourth moment of the population distribution.  Notably, for a Gaussian population
distribution this reduces to $var[hat(sigma)^2] = (2 sigma^4)/(n-1)$.  In either case, it is useful
to know that the variance of this estimator decreases as $1 slash n$, which is the same as for
$var[hat(mu)]$.

== Maximum Likelihood Estimation (MLE)
To fit a theory with parameters $theta$ to observation data $x$, we should maximize the probability
$P(x|theta)$.  This procedure is known as a *likelihood fit*, or *maximum likelihood estimation*.
Usually, rather than maximizing $P(x|theta)$ directly, we maximize its logarithm, and define the
*log likelihood function*
$ ell(theta) = log[P(x|theta)] $
The maximum likelihood estimate is then
$ hat(theta) = arg max_theta ell(theta) $
The logarithm is useful for numerical stability, and because many probability distributions are
either exponential, can be easily factorized, or both.

=== With Gaussian Noise
#let obsseq = $O = {(x_1, y_2), (x_2, y_2), ..., (x_n, y_n)}$
Consider, as a hypothesis
$ Y = f(X; theta) + epsilon $<mle-hypot>
where $X$, $Y$ and $epsilon$ are random variables.  By assumption $epsilon tilde cal(N)(0,sigma)$ is
independent of both $X$ and $Y$.  $theta$ is a set of model parameters which we wish to estimate by
maximizing the probability of a sequence of observations #obsseq.

Since $Y - f(X;theta) = epsilon$, the random variable $Y - f(X;theta) tilde cal(N)(0,sigma)$.
By assumption, each observation $(x, y)$ is a sample of $(X, Y)$.  Then
$ P(O|theta) prop 
  product_(j=1)^n exp[-1/2 ((y_j - f(x_j; theta))/sigma)^2]
$
We will not bother computing the normalization factor for $P(O|theta)$ since our goal will be to
minimize $ell(theta)$, which we will define without regard to normalization
$ ell(theta) = sum_(j = 1)^n ((y_j - f(x_j;theta))/sigma)^2 $<log-lik-gauss-specific>
This tells us that the maximum likelihood model can be determined by minimizing the sum of squares
of the model error for all observations $y_j - f(x_j; theta)$, weighted by the variance $sigma$.  In
realistic cases, the assumed error variance $sigma^2$ is not necessarily the same for each
observation.  We can generalize to this case by assuming $Y_j = f(X_j; theta) + epsilon_j$ where the
$X_j$'s and $Y_j$'s are by hypothesis each distributed the same as $X$ and $Y$ respectively, but the
$epsilon_j tilde cal(N)(0, sigma_j)$.  Therefore, we can generalize @log-lik-gauss-specific to
$ ell(theta) = sum_(j = 1)^n ((y_j - f(x_j;theta))/sigma_j)^2 $<log-lik-gauss>

By minimizing $ell$ with respect to $theta$ we now have a general formual for fitting model
parameters $theta$ to observations.  This required us to assume the distribution of $epsilon$, but
notably it did not require us to assume any particular form of $f$.  This formula is therefore valid
for any hypothesized $f$.  In the case where $f(x;theta)$ is linear in both $x$ and $theta$, the
model which minimizes $ell(theta)$ is known as a *linear regression*.

=== Of a Binary Variable
Now we suppose instead that $Y : Omega -> ZZ_2$ and $X : Omega -> RR$.  We have already seen that
the unique probability distribution for a sample set with cardinality 2 is the Bernoulli
distribution, which has a single parameter $p$.  We take as our hypothesis
$ Y tilde "Bern"(p) wide p = f(X;theta) $
so that
$ P(O|theta) prop
  product_(j=1)^n f^(y_j)(x_j;theta) (1 - f(x_j;theta))^(1 - y_j)
$
Then, taking the log of the Bernoulli distribution, up to a constant we have
$ ell(theta) = sum_(j=1)^n [y_j log(f(x_j;theta)) + (1 - y_j) log(1 - f(x_j;theta))] $
For this to make sense, we must have $0 <= f(x;theta) <= 1$.  One possible choice for $f$ is
$ f(x; theta) = 1/(1 + e^(-h(x;theta))) $<logit-func>
where $h : RR -> RR$.  The function $(1 + e^(-x))^(-1)$ is known as the *logit* function.  The model
which minimizes $ell$ in the case where $h(x;theta)$ is linear in both $x$ and $theta$ is called a
*logistic regression*.  Note that
$ h(x;theta) = log[ f(x;theta) / (1 - f(x;theta)) ] $
Since $f$ is playing the role of a probability (the parameter $p$ of the Bernoulli distribution),
$h$ is also known as the *log odds*.

Our choice of $f$ was of course completely arbitrary.  There are other common choices, for example
$ f(x;theta) = 1/2 [1 + erf(h(x;theta))] $
where $h$ is linear in both $x$ and $theta$ (in other words, $f$ is the cumulative probability
distribution of a Gaussian). The CLT serves as motivation for this choice.  This is called a *probit
regression*.  $f(x;theta)$ and $Phi$ are qualitatively similar, but the former is somewhat easier to
deal with.  The similarity seems notable because the logistic regression is far more common than the
probit regression, but only in light of the latter does the motivation for the former become
apparent.

=== Generalized Linear Models
Generalizing the previous two examples, we can write
$ expect(Y)_epsilon = g^(-1)(beta X) $
The expectation value on the left hand side is to be taken over stochastic noise assumed to connect
the model to observations, which here and in the Gaussian example we denote $epsilon$.  $X$ can have
any number of dimensions and $beta$ is a linear operator.  The function $g : RR -> RR$ is called a
*link function*.  We can think of this as a function of a "mean value" given in terms of $X$, for
example if $g = bold(1)$, the expectation value of $Y$ is simply $beta X$ as in our first example.

Technically, the distribution of $epsilon$ over which we take $expect(Y)$ needn't depend on the link
function $g$.  However, the range of $g$ can inform our choice.  In our first example, $"range"(g) =
RR$, so we chose $epsilon tilde cal(N)(0, sigma)$.  In the logistic regression, the $"range"(g) =
[0,1]$ so we chose a Bernoulli distribution, though we could just as easily have chosen the beta
distribution.  Some common pairings of distribution and link function are shown in @tab-link-funcs.

#figure(
  table(
    columns: 4, stroke: 0.02em,
    [*Distribution*], [*Support*], [*Link Function* $g(mu)$], [*Mean* $expect(Y)$],
    $cal(N)(mu, sigma^2)$, $RR$, $1$, $beta X$,
    $Gamma(alpha, beta)$, $RR_gt.eq$, $-1/mu$, $-(beta X)^(-1)$,
    $"Pois"(lambda)$, $ZZ_gt.eq$, $log(mu)$, $e^(beta X)$,
    $"Bern"(p)$, $ZZ_2$, $log(mu/(1-mu))$, $(1 + e^(-beta X))^(-1)$,
    $"Binom"(n,p)$, $ZZ_n$, $log(mu/(n - mu))$, $(1 + e^(-beta X))^(-1)$,
  ),
  caption: [Common choices of link function.],
)<tab-link-funcs>

=== Empirical Risk Minimization (ERM)
In the section on maximum likelihood estimation we have repeatedly, without explicit justification,
split the distribution of observed data into the variables $X$ and $Y$.  While technically not a
requirement, the reason for doing this is that experiments typically have some controlled variable
represented by $X$ and some response variable represented by $Y$.  These must fall in some joint
probability distribution $p_(X Y)(x,y)$.  Another way of finding a model of best fit is to define
some *loss function* $L$ and minimize its expectation, for example
$ expect(L(Y, f(X;theta))) = integral dif x dif y thin p_(X Y)(x, y) thin L(y, h(x;theta)) $
Typically $L$ is a function that characterizes the "badness" of a model described by the function
$f(dot;theta)$, for example the mean squared error.  Typically the integral is estimated by summing
over observed $(x, y)$ pairs and the model parameters $theta$ are determined by minimizing
$expect(L)$.


== Monte Carlo Integration
Consider the probability distribution
$ p(x) = cases(
    1/"vol"(V) &quad arrow.l.double x in V,
    0 &quad arrow.l.double x in.not V,
  )
$<monte-carlo-flat>
for $x in RR^n$ with $V subset RR^n$ and $"vol"(V)$.  Then, for any function $f : RR -> RR^n$
$ expect(f(X)) = integral_V dif^n x thin f(x) $
We can exploit this relationship to compute the integral $integral_V dif^n x thin f(x)$ using an
estimator for the mean of $f$ with respect to the random variable $X$.  As we have already seen, one
such estimator is
$ hat(f) = 1/m sum_(j=1)^m f(x_j) $
As we have seen $var[hat(f)] prop m$, so that the statistical uncertainty in our integral decreases
as $1 slash m$.

While this method of integration may seem very inefficient, note that the error scales with $1 slash
m$ _regardless of the number of dimensions_ $n$.  This method, called *monte carlo integration*, is
therefore very efficient in large dimensions.

Samples drawn where $f(x)$ is small must have a small contribution to $expect(f(X))$, so we'd like
to avoid sampling too much in those regions.  We can improve the method by exchanging
@monte-carlo-flat for something else.  We do this by, instead of evaluating $expect(f(X))$,
evaluating
$ expect(f(X')/(p_(X')(X'))) = integral_V dif^n x thin p_(X')(x) (f(x)/(p_(X')(x))) =
  integral_V dif^n x thin f(x)
$
While the left hand side looks peculiar because of the presence of $p_(X')$ inside the expectation
value, it is merely a function, so we are free to compute whatever expectation value we like.  From
this we see that an improved estimator is
$ hat(f)' = 1/m sum_(j=1)^m f(x_j)/(p_(X')(x_j)) $
for which $var[hat(f)'] = var[f(X') slash p_(X')(X')] slash m$.  This of course scales with $1 slash
m$ just as $var[hat(f)]$ does, but we can now try to reduce the variance by choosing a $p_(X')$
which maintains as close to constant ration with $f$ as possible.  One might be tempted to then
simply pick $p_(X')(x) prop f(x)$, but to do this we'd have to normalize $f(x)$, which requires
computing its integral, which is what we're trying to do in the first place.  Despite this, way may
still be able to significanltly reduce the variance of the estimator by choosing a $p_(X')$ which
can be efficiently sampled from.
