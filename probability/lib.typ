#let conf(
  title: none,
  authors: (),
  abstract: none,
  contents: false,
  equation-numbers: "(1)",
  section-numbers: "1.1 ",
  page-numbers: none,
  heading-spacing: 10pt,
  font-size: 9pt,
  latex-equation-refs: true,
  doc,
) = {
  set text(font: "New Computer Modern", size: font-size)
  set math.equation(numbering: equation-numbers)
  set heading(numbering: section-numbers)
  set page(numbering: page-numbers)

  // for pdf metadata
  set document(title: title)

  // equation refs in parentheses as in latex
  // yes, it's annoying that this is so complicated
  show ref: it => {
    let eq = math.equation
    let el = it.element
    if latex-equation-refs and el != none and el.func() == eq {
      numbering(el.numbering, ..counter(eq).at(el.location()))
    } else {
      it
    }
  }

  set align(center)
  text(weight: "medium", size: 20pt, title)

  if type(authors) != array {authors = (authors,)}
  grid(
    columns: (1fr,) * authors.len(),
    row-gutter: 20pt,
    ..authors.map(author => {
      if type(author) == str {author = (name: author)}
      // for some weird reason it doesnt allow backslash without new line
      [#author.name #linebreak()]
      if "affiliation" in author.keys() [#author.affiliation #linebreak()]
      if "email" in author.keys() [#link("mailto: " + author.email)]
    }),
  )

  if abstract != none {
    v(heading-spacing)
  
    par(justify: false)[
      *Abstract* \
      #abstract  
    ]
  
    v(heading-spacing)
  }

  set align(left)

  show outline.entry.where(level: 1): it => {
    v(12pt, weak: true)
    strong(it)
  }

  if contents {
    outline(title: text(size: 12pt, [Contents]),
            indent: 1em,
            fill: repeat([#h(0.25em).#h(0.25em)]),
           )
  }

  v(heading-spacing)

  columns(1, doc)
}

//NOTE: install otf-latin-modern and otf-latinmodern-math

#let appendix(
  // of the entire appendix, not of an individual appendix
  title: [Appendix],
  spacing: 10pt,
  numbering: "A.1 ",
  doc,
) = {
  v(spacing)
  counter(heading).update(0)
  set heading(numbering: none, supplement: [Appendix])
  heading(text(weight: "bold", title))
  counter(heading).update(0)
  set heading(numbering: numbering)
  doc
}

// args can be same as table input,
// dictionaries with keys symbol, desc, sort
// will sort entries according to sort (e.g. string or number)
// (this isn't working quite right because I don't know WTF ordering typst uses to sort strings)
#let glossary(stroke: none, title: [Glossary of Symbols], ..txt) = {
  txt = txt.pos()
  if txt.len() != 0 and type(txt.first()) == dictionary {
    txt = txt.sorted(key: d => d.at("sort", default: ""))
    txt = txt.map(d => (d.symbol, d.desc)).flatten()
  }
  [#heading(numbering: none, title)
   #table(stroke: stroke, columns: 2, ..txt)
  ]
}

#let units(x, spacing: 0.2em) = {
  show math.frac: l => $#l.num slash #l.denom$
  $#h(spacing) upright(#x)$
}

#let bra(x) = $lr(|#x angle.r)$
#let ket(x) = $lr(|#x angle.l)$
#let braket(x,y) = $lr(angle.l #x mid(|) #y angle.r)$
#let expect(x) = $lr(angle.l #x angle.r)$

#let TODO(txt) = highlight[#underline[*TODO*] #txt]

#let var = $"var"$
#let cov = $"cov"$
#let erf = $"erf"$
#let bar = math.overline
