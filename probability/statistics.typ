#import "lib.typ": *

= Statistics
A statistic is a functional of a probability density.  In a sense, they describe the infinite
dimensional space of all possible probability densities in finite dimensional terms.

== Expectation Value
Arguably the most important, and certainly the most common type of statistic is known as an
*expectation value*.  It is defined by

$ expect(f(X)) = integral_Omega dif x thin f(x) thin p_(X)(x) $<expect>

On a discrete set we can write

$ expect(f) = sum_(A_j in Omega) f(A_j) thin P(A_j) $

where the $A_j$ are elements of a partition of $Omega$, that is $union.big_j A_j = Omega$, $A_i sect
A_j = emptyset, forall i != j$.

The variety of notations available for expectation values is bewildering.  Here we will use
$expect(dot)$, which is most common among physicists.  Mathemeticians will often use $E(dot)$,
$E[dot]$, or $EE[dot]$.  We will adopt the convention of writing expressions inside the
$expect(dot)$ as functions of random variables, such that the expectation is an integral over
densities of those variables.  In contexts where it may be confusing what we are integrating over,
we will write the name of the random variable being integrated over in the subscript, e.g.
$expect(dot)_X$.

It should be obvious that for any constant $c$, $expect(c) = c$.  The simplest non-trivial
expectation value is then $expect(X)$, which is called the *mean*.  That is $expect(X)$ is the mean
of the probability density $p_(X)(x)$.

Some simple examples are in order.  For $X tilde delta(x - x_0)$, we have $expect(X) = x_0$, since
no other value is supported by the density.  For a "flat" distribution
$ p(x) = cases(1 quad arrow.double.l x in [0,1], 0 quad arrow.double.l x in.not [0,1]) $<flat-dens>
we compute
$ expect(X) = integral_0^1 dif x thin x = lr(x^2/2|)_(x=1) = 1/2 $
which conforms with the intuitive notion of the value "in the middle" of $p_X$.  It should be
obvious, but we nonetheless emphasize, that $expect(X)$ _need not_ be a "likely" value of $X$ in
any sense.  For example, for $X tilde (delta(x) + delta(x-1))/2$ we have $expect(X) = 1/2$, but we
cannot sample any values in a neighborhood of $1/2$.  $expect(X)$ is often interpreted as providing
a "typical value" of $X$, though, as we have seen, this interpretation has limitations depending on
the distribution.

We can sompute $expect(f(x))$ for any function $f$ for which the integral converges, but some of
these statistics are so commonly used that they have special names.  The *variance* is
$ var[X] := expect((X - expect(X))^2) $
which we can simplify to
$ var[X] = expect(X^2) - 2 expect(X expect(X)) + expect(X)^2 = expect(X^2) - expect(X)^2 $
Sometimes the symbol $sigma^2$ is associated with the variance, for example we may write $sigma_X^2
= var[X]$.  $sigma_X = sqrt(var[X])$ is called the *standard deviation*.

Similarly
$ m_n = expect((X - expect(X))^n)/sigma_X^n $
are known as *moments* of the distribution of $X$.  $m_3$ is also called the *skewness* and $m_4$ is
also called the *kurtosis*.

== Mode
A *mode* is a point at which a distribution reaches its supremum
$ arg sup_x p(x) $
Distributions with a unique mode are called *unimodal*, those with exactly two distinct modes are
*bimodal*.  Of course the modes can be computed by solving $diff_x p(x) = 0$ and checking which
results are maxima.

== Quantiles
*Quantiles*, along with the mode, are arguably the only commonly used statistics which are not
expressible as expectation values.  They are only relevant in cases where $Omega$ is isomorphic with
$RR$, in which sense they are univariate statistics only.  Consider

$ 1/q = integral_xi^oo dif x thin p_(X)(x) $

The quantile in this expression is $xi$, which is the value such that "the rest of the distribution
after $xi$" makes up $1 slash q$ of the total probability.  More generally, we can define a set
${xi_0,xi_1...,xi_q}$ such that

$ 1/q = integral_(xi_k)^(xi_(k+1)) dif x thin p_(X)(x) $

That is, we can partition the interval on $RR$ in which $p_X$ has support into $q$ parts, each with
an equal total probability of $1 slash q$.  As an example, a $4$-quantile is also called a
*quartile*, using @flat-dens restricted to $[0,1]$ as an example we have

$ xi_0 = 0 wide
  xi_1 = 1/4 wide
  xi_2 = 1/2 wide
  xi_3 = 3/4 wide
  xi_4 = 1
$

Alternatively, if we continue $p_(X)(x)$ to all of $RR$, keeping its value at $0$ outside of
$[0,1]$, we'd have $xi_0 = -oo$ and $xi_4 = oo$.  The $0$th and $q$th $q$-quantiles are always the
minimum or maximum values on which the distribution has support, and can always be taken to be
$plus.minus oo$, for which reason they are usually not referred to.  This means that there are
always $q-1$ non-trivial $q$-quantiles.  In terms of the cumulative probability $F_X$ we have

$ 1/q = F_(X)(xi_(k+1)) - F_(X)(xi_k) $

There is only a single finite $2$-quantile and this is referred to as the *median*.  It's worth
noting that the median is often used along with or in place of the mean as indicating a "typical
value", but it is less sensitive to outliers in the sense that a distribution which falls off only
slowly toward $plus.minus oo$ will tend to have a much larger (as in much more positive or much more
negative) mean than median.

If the sample set $Omega$ has some concept of ordering we can define quantiles also for discrete
sets, but we cannot in general guarantee that all of the quantiles are defined.  For example,
consider the set ${0,1,2}$, with $P(0) = 1/2$, $P(1)=P(2)=1/4$.  Here $1$ is the median since $P({x
| x >= 1}) = P(1) + P(2) = 1/2$.  $2$ is the largest quartile since $P({x | x >= 2) = P(2) = 1/4$.
There are however, no $3$-quantiles, since there is no $xi in {0,1,2}$ such that $P({x|x >= xi}) =
1/3$.  This can only occur for discrete sets or discontinuous probability densities. All
$q$-quantiles for all $q >= 2$ exist for continuous densities.

Some fields of study make frequent use of $100$-quartiles which are also known as *percentiles*.

As we have previously mentioned, the generalization of quantiles to higher dimensions is not
striaghtforward.  A quantile in $n$ dimensions is an $n-1$ dimensional hypersurface, but it is also
not in general unique.  We can, however, talk about percentiles of $1$-dimensional marginal
probability distributions.  For example, if we have some density $p_(X Y)(x,y)$, we can discuss
quantiles of $p_X$ and $p_Y$, that is, we let one of the coordinates define the hypersurfaces which
we take as the quantiles.

== Multivariate Statistics
In most of the preceding discussion we have avoided explicitly specifying whether $Omega$ has one or
more dimensions.  Indeed, the generalization of @expect to multiple dimensions is trivial.
Nevertheless, there are some common notations, conventions and terminology specific to multivariate
statistics, so we will review them here.  We will denote multidimensional random variables with an
index, e.g. $X^i$, which unless otherwise specified we will take to be vectors on $RR^n$.

The obvious generalization of the mean is

$ expect(X^i) = integral dif^n x thin x^i thin p_(X)(x) $

The probability density $p_X$ here is no different than more explicitly multivariate densities we
have already seen such as $p_(X Y)(x,y)$ except with a more compact notation.  We will often write
$mu_X^i := expect(X^i)$, or simply $mu^i$ for convenience, when there is no risk of confusion.

While the generalization of the mean to higher dimensions is a vector, the generalization of the
variance is a symmetric matrix

$ Sigma_X^(i j) := expect((X^i - expect(X^i)) (X^j - expect(X^j))) $

This is referred to as the *covariance* matrix.  When written in terms of two scalar random variables $X$
and $Y$, this is often written

$ cov[X,Y] := expect((X - expect(X)) (Y - expect(Y))) $

which is of course a scalar.  We can extend to higher dimensional $X$ and $Y$

$ cov[X^i, Y^alpha] = expect((X^i - expect(X^i)) (Y^alpha - expect(Y^alpha))) $

where we have written $X$ and $Y$ with different types of indices to emphasize that they needn't be
of the same number of dimensions to define the covariance.  In such a case the covariance matrix is
non-square.  $Sigma_X^(i j) = cov[X^i, X^j]$ is sometimes called an *auto-covariance* matrix.

The *correlation* coefficient, is simply the covariance scaled by the standard deviation of each
variable, for example
$ "corr"[X, Y] := cov[X,Y]/(sigma_X sigma_Y) $
which is only worth mentioning because it is frequently referred to.

Let's take a moment to examine how the elements of $Sigma^(i j)$ should be interpreted. Clearly the
diagonal elements are merely the variances of each component.  The off-diagonal terms can be written

$ cov[X,Y] = expect(X Y) - mu_X expect(Y) - mu_Y expect(X) + mu_X mu_Y = expect(X Y) - mu_X mu_Y $

We know that

$ expect(X Y) = integral dif x dif y thin x y thin p_(X Y)(x, y) $

When $X$ and $Y$ are independent we can factorize this into

$ (integral dif x thin x p_(X)(x))(integral dif y thin y p_(Y)(y)) = mu_X mu_Y $

in which case $cov[X,Y] = 0$.  In the other limiting case, where $X = Y$, we have $cov[X,Y] =
expect(X^2) - mu_X^2 = var[X] = var[Y]$.  Therefore, random variables with independent components
have diagonal covariance matrices, and as we increase their correlation we approach a uniform matrix
with all elements equal to $var[X]$.

== Characteristic Functions
The *characteristic function* of a random variable $X$ is the Fourier transform of its probability
density
$ phi.alt_(X)(k) = integral dif x thin e^(i k x) p_(X)(x) = expect(e^(i k X)) $<char-func>
Basic facts about Fourier transforms tell us that this can be inverted
$ p_(X)(x) = integral (dif k)/(2 pi) e^(-i k x) phi.alt_(X)(k) $
Characteristic functions are useful mainly as an analytical tool the same way that Fourier
transforms are useful more generally.  As an important example, consider the sum of two random
variables $Z = X + Y$.  As we have seen in @sum-rand-vars, the probability density of $Z$ is the
convolution of those of $Y$ and $X$.  We can then write
$ p_(Z)(z) &= integral dif x integral (dif k)/(2 pi) integral (dif l)/(2 pi)
  e^(-i l z) e^(i l x) e^(-i k x) phi.alt_(X)(k) phi.alt_(Y)(l) \
  &= integral (dif l)/(2 pi) e^(-i l z)
  (integral dif x integral (dif k)/(2 pi) e^(i (l - k) x) phi.alt_(X)(k) phi.alt_(Y)(l))
$
From this and the definition @char-func, we can simply read off the characteristic function for $Z$
$ phi.alt_(Z)(l) =
  integral dif x integral (dif k)/(2 pi) e^(i (l - k) x) phi.alt_(X)(k) phi.alt_(Y)(l)
$
Recognizing that the Fourier transform of a plane wave is the $delta$ function
$ delta(x) = integral (dif k)/(2 pi) e^(i k x) $
we find
$ phi.alt_(Z)(l) = integral dif l thin delta(k - l) phi.alt_(X)(k) phi.alt_(Y)(l) =
  phi.alt_(X)(l) phi.alt_(Y)(l)
$
More succinctly, for posterity
$ phi.alt_(X+Y)(k) = phi.alt_(X)(k) phi.alt_(Y)(k) $<char-func-sum>
That is, the characteristic of the sum of random variables is the product of their characteristic
functions.  This should be familiar to anyone used to Fourier transforms as the convolution theorem,
which says essentially the same thing in somewhat different language.

The above can be trivially generalized to arbitrarily many variables, and to a general linear
combination of those variables by re-scaling
$ phi.alt_(a_1 X_1 + dots.c + a_n X_n) = phi.alt_(X_1)(a_1 k) dots.c phi.alt_(X_n)(a_n k)
$<char-func-lin-comb>

Note also that from $phi_(X)(k) = expect(e^(i k X))$, we can expand the exponential.  We can always
shift a random variable to have zero mean by translation $X -> X - mu$ so that
$ phi_(X)(k) = expect(e^(i k X)) = e^(i k mu) expect(sum_(n=0)^oo (i^n k^n (X - mu)^n)/(n!))
  = e^(i k mu) sum_(n=0)^oo (i^n k^n sigma^n m_n)/(n!)
$
where $m_n = expect((X - mu)^n) slash sigma^n$ is the $n$th moment.  Note that the $n=1$ term always
vanishes, by construction.  We could have redefined $X -> X' = (X - mu)/sigma$ to eliminate both the
phase $e^(i k mu)$ and the $sigma$, which is sometimes convenient.  We will see in our discussion of
some specific distributions that this expansion is sometimes useful.
