#import "lib.typ": *

#let abstract = [
  A simple review of the fundamentals of probability theory.
]

#show: doc => conf(
  title: [Expanding Man's Probability Review],
  authors: "Expanding Man",
  page-numbers: "1",
  contents: true,
  abstract: abstract,
  doc,
)


= Introduction
Probability, in addition to being fundamental to most areas of science, has immediate relevance to
everyday life.  Despite this, probability theory did not see significant development until the late
renaissance, later even than differential calculus, which perhaps lends support to its reputation
for being subtle and unintuitive.

In this review I will give a self-contained description of probability theory and some of its
simplest applications.


#include "fundamentals.typ"
#include "statistics.typ"
#include "distributions.typ"
#include "estimators.typ"
#include "processes.typ"

//#bibliography(full: true, "refs.yml")
