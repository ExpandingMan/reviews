#import "lib.typ": *

#let disttable(..args, caption: none) = {
  figure(caption: caption,
    table(
      stroke: silver,
      fill: luma(240),
      columns: 5,
      [Name], [Parameters], [Support], [Mean], [Variance],
      ..args
    )
  )
}

= Distributions
Here we discuss the properties of certain specific classes of probability density function.  As we
will see, one of these in particular, the Gaussian or normal distribution, acts as a kind of
limiting case of all other distributions, in a sense that we will see below.  Many of the other most
important examples of specific classes of distributions are formed by combining Gaussians in some
way.

== Gaussian Distribution
#disttable(
  $cal(N)(mu, sigma^2)$, $mu in RR, sigma > 0$, $RR$, $mu$, $sigma^2$,
)

The *Gaussian* or *normal* distribution is uniquely important in a sense we will describe below.
The univariate Gaussian distribution has the form
$ X tilde cal(N)(mu, sigma) ==> wide
  p_(X)(x) = 1/(sigma sqrt(2 pi)) e^(- 1/2 ((x - mu)/sigma)^2)
$
where $mu in RR$ and $sigma in RR_+$ are parameters.  Their names are not coincidental, since
$ expect(X) &= mu \
  var[X] &= sigma^2
$
The factor $(sigma sqrt(2 pi))^(-1)$ should be thought of as simply a normalization factor for
ensuring $integral dif x thin p_(X)(x) = 1$.  Note that the Gaussian is symmetric about $x=mu$.  As
a consequence, its skewness, and all of its odd-numbered moments vanish.

Computing the cumulative probability of a Gaussian is non-trivial.  Due to the importance of the
distribution, a transcendental function called the *error function* is defined
$ erf(x) = 2/sqrt(pi) integral_0^x dif xi thin e^(-xi^2) $
This is simply an integral over half the Gaussian after a change of variables.  This function is the
subject of a great deal of study in its own right, and its properties are extensively documented.
The cumulative probability is therefore
$ F_(x)(x) = 1/2 [1 + erf((x - mu)/(sigma sqrt(2)))] $

Arguably the property of this distribution that makes it so simple is that the characteristic
function of a Gaussian is also a Gaussian up to a normalization factor
$ phi.alt(k) = e^(i k mu) e^(- 1/2 sigma^2 k^2) $<gauss-char>
From this and @char-func-sum we can conclude that for two random variables $X,Y tilde
cal(N)(mu,sigma)$
$ Z = X + Y ==> wide Z tilde cal(N)(mu, sqrt(2)sigma) $
This is trivially generalized to the case of arbitrarily many independent i.i.d. random variables.
This is an exact special case of the more general central limit theorem which we discuss below.

=== Central Limit Theorem
The *Central Limit Theorem* (CLT) is arguably the single most important result in statistics, and as
we will briefly discuss below one of the things that makes empirical science possible.

Let ${X_1,X_2,...,X_n}$ be a set of i.i.d. random variables.  A natural question is what is the
distribution of their sum, that is, the distribution of
$ bar(X)_n := (X_1 + X_2 + thin dots.c thin + X_n)/n $

We should expect that the distribution of $bar(X)_n$ depends on the distribution of each of the
$X_j$.  Naively, we might also expect that this is true even in the limit $n -> oo$.  Remarkably, as
we will show, this is not the case, instead the distribution of $bar(X)_n$ is _always_ a Gaussian,
regardless of the distribution of each of the contituent variables.

To see how this occurs, we will make use of characteristic functions.  Recalling the formula for the
characteristic function of a linear combination of random variables @char-func-lin-comb, we write
$ phi.alt_(bar(X)_n)(k) =
  sqrt(n) product_(j=1)^n phi.alt_(X_j)(n^(-1/2) k) = sqrt(n) phi_(X)^(n)(n^(-1/2) k)
$
where $phi.alt_X$ is the common characteristic function of all the $X_j$'s.  We have inserted the
factor of $sqrt(n)$ arbitrarily using @char-func-lin-comb for reasons that will soon become
apparently.  Without loss of generality, we can choose each $expect(X_j)=0$ and $var[X]=1$ (since
this is just a translation and re-scaling, it should be trivial to transform them back at the end of
any calculation).  Therefore
$ phi.alt_(bar(X)_n)(k) = sqrt(n) (1 - k^2/(2 n) + cal(O)(n^(-3/2) k^3))^n =
  sqrt(n) (1 - k^2/(2 n))^n + cal(O)(n^(-3/2) k^3)
$
Using $e^x = lim_(n -> oo) (1+x/n)^n$, we find
$ phi.alt_(bar(X)_n) = sqrt(n) thin e^(-k^2/2) + cal(O)(n^(-3/2) k^3) $
As we have seen, the characteristic function of a Gaussian is itself a Gaussian, which is what we
have here up to corrections of order $n^(-3/2)k^3$.  So we have shown that
$ lim_(n -> oo) sqrt(n)/sigma (bar(X)_n - mu) tilde cal(N)(0,1) $
or, equivalently, after shifting by $mu$ and scaling by $sigma/sqrt(n)$
$ lim_(n -> oo) bar(X)_n tilde cal(N)(mu, sigma slash sqrt(n)) $
That is, the arithmetic mean of i.i.d. random variables, each with mean $mu$ and $sigma^2$
approaches a Gaussian with mean $mu$ and variance $sigma^2 slash n$.  Crucially, this does _not_
depend on the distribution of each variable in the sum.

Unsurprisingly, the mean of $bar(X)_n$ is simply $mu$, the mean of each contributing variable.  The
variance of $bar(X)_n$ however is _less_ than the variance of each $X_j$.  It's worth emphasizing
that this is true of the _mean_ $bar(X)_n$, but _not_ the sum $X_1+dots.c+X_n$.  An consequence of
this with important practical implications is that, given any random process which we can sample
freely, we can always construct a statistic with arbitrarily small variance.  While $bar(X)_n$
differs from a Gaussian by the fourier transform of the $cal(O)(n^(-3/2) k^3)$ terms in the general
case, in the case where the population distribution is also Gaussian, these correction terms vanish,
and the distribution of the mean or sum is _always_ a Gaussian with variance exactly $n sigma^2$ for
all values of $n$ (and cosequently the variance of the mean is always $sigma^2 slash n$).

The CLT plays a special role in science in that it allows us to obtain results that do not depend on
detailed knowledge of probability distributions, in particular the uncertainty distributions of
measurements.  It also provides with a universal algorithm for reducing certain types of
uncertainty: take more measurements.  Since the variance of the mean is smaller than the variance of
the population distribution, our confidence in the mena can be higher than our confidence in any
individual measurement.  This provides us with rigorous justification for the idea that taking more
measurements reduces statistical uncertainties.

=== Multivariate Gaussian Distribution<sec-multivar-gaussian>
#disttable(
  $cal(N)_(n)(mu, Sigma)$, $mu in RR^n, Sigma in RR^n times RR^n$, $RR^n$, $mu$, $Sigma$,
)
Generalizing $cal(N)(mu,sigma^2)$ to higher dimensions is fairly trivial, but it is such an
important case that we will take a moment to review it.  We denote a set of random variables $X :
Omega -> RR^n$ that are distributed according to a multivariate Gaussian distribution as
$ X tilde cal(N)_(n)(mu, Sigma) $
where $mu in RR^n$ and $Sigma in RR^n times RR^n$ is the covariance matrix.  This is precisely the
covariance matrix we have already introduced $Sigma^(i j) = cov[X^i,X^j]$.  It is left as an
exercise for the reader to show that $X tilde cal(N)_(n)(mu, Sigma) => Sigma^(i j) = cov[X^i,
X^j]$.  The probability density function can be obtained via normalization and is given by

$ cal(N)_(n)(mu, Sigma) = 1/sqrt((2 pi)^k det(Sigma))
  exp(-1/2 (x - mu)^upright(T) Sigma^(-1) (x - mu))
$

Another useful fact about multivariate Gaussian variables is that the marginal distribution of the
$i$th component variable is itself a Gaussian with variance $Sigma^(i i)$.  More generally, the
marginal distribution for any subset of variables is simply the multivariate Gaussian with the
variables in the complement of the subset deleted.  For example, given variables $(X^1, X^2, X^3)
tilde cal(N)(mu, Sigma)$, the marginal distribution for $(X^1, X^2)$ is simply $cal(N)((mu^1, mu^2),
Sigma_((1 2)))$ where $Sigma_((1 2))$ is $Sigma$ with the third row and column deleted.  Showing
this is tedious but can be done directly by integration.

Conditional distributions of the component variables are also Gaussian, but less straightforward.
Suppose we partition $X = vec(X_1, X_2)$ where $X_1 : Omega -> RR^q$ and $X_2 : Omega -> RR^(n-q)$.
Accordingly we write

$ mu = vec(mu_1, mu_2) #h(6em)
  Sigma = mat(Sigma_(1 1), Sigma_(1 2); Sigma_(2 1), Sigma_(2 2))
$

Then $p_(X_1 | X_2)$ is a multivariate Gaussian with

$ mu' = mu_1 + Sigma_(1 2) Sigma_(2 2)^(-1) (X_2 - mu_2) \
  Sigma' = Sigma_(1 1) - Sigma_(1 2) Sigma_(2 2)^(-1) Sigma_(2 1)
$<conditional-multivar-gaussian>

As Gaussian distributions are so common in nature, thanks in no small part to CLT, the multivariate
Gaussian distribution has many important applications, some of which we will explore later.


== $Gamma$ Distribution
The class of $1$-dimensional distributions which maximize entropy for a fixed $expect(X)$ is called
the $Gamma$ distribution.  We will leave the formal introduction of entropy to sections on
information theory, and focus on the $chi^2$ distribution, an important special case of $Gamma$
distribution.

=== $chi^2$ Distribution
#disttable(
  $chi^2(k)$, $k in ZZ_>$, $RR_gt.eq$, $k$, $2 k$,
)
The $chi^2$ statistic is defined as
$ Q = sum_(j=1)^k (X_j - mu_j)^2/sigma_j^2 $<chi2-stat>
where $X_j$ are i.i.d. random variables, $mu_j = expect(X_j)$ and $sigma_j = var[X_j]$.  In the
special case where the $X_j tilde cal(N)(mu_j, sigma_j)$, $Q tilde chi^2(k)$ where $chi^2(k)$ is
known as the *$chi^2$ distribution* with $k$ degrees of freedom.  It is given by
$ p_(Q)(x) = (x^(k/2 - 1) e^(-x/2))/(2^(k/2) Gamma(k slash 2)) wide forall x >= 0 $<chi2-dist>
where $Gamma(x)$ is the $Gamma$ function ($Gamma(n) = (n-1)!$ for $n in ZZ_gt.eq$).  We will not
derive this here, but suffice it to say that it can be derived from @chi2-stat and characteristic
functions.

The $chi^2$ distribution is useful in hypothesis testing.  If we hypothesize that a sequence of
random variables $X_j$ are normally distributed, by definition their $chi^2$ statistic should fall
in this distribution.  We can use @chi2-dist to compute the cumulative probability of this the value
obtained, which we expect to be roughly $k$.  If the value we obtain for $Q$ is much smaller than
$k$, it is an indication that the $X_j$ have a more sharply peaked distribution than we expected
(perhaps indicating that our predictions were "suspiciously good"). If the value we obtain is much
greater than $k$, it indicates that our predictions are poorer than expected, perhaps ruling out our
fit.

=== General $Gamma$ Distribution
#disttable(
  $Gamma(alpha,beta)$, $alpha > 0, beta > 0$, $RR_gt.eq$, $alpha/beta$, $alpha/beta^2$,
)
We have seen that the $chi^2$ distribution is of the form $x^a e^(-b x)$.  The class of
distributions of this form, with proper normalization are $Gamma$ distributions, with the general
form
$ p(x) = (x^(alpha - 1) e^(- beta x) beta^alpha)/(Gamma(alpha)) wide forall x >= 0 $
Note that we are somewhat confusingly re-using the symbol $Gamma$ for both the $Gamma$ function and
the $Gamma$ distribution.

== $Beta$ (Beta) Distribution
#disttable(
  $Beta(alpha,beta)$, $alpha > 0, beta > 0$, $[0,1]$, $alpha/(alpha+beta)$,
  $(alpha beta)/((alpha + beta)^2 (alpha+beta+1))$,
)
I will mention the $Beta$ distribution only briefly (this should be read as an uppercase $beta$, not
the latin capital "B").  This distribution is defined only on $[0,1]$, and is given by
$ p(x) = (Gamma(alpha + beta))/(Gamma(alpha) Gamma(beta)) x^(alpha - 1) (1 - x)^(beta - 1) wide
  forall x in [0,1]
$
This distribution is notable mostly because it is a class of analytic distributions on $[0,1]$.  It
is frequently used as a Bayesian prior distribution for parameters on finite intervals.

== Bernoulli Distribution
#disttable(
  [$"Bern"(p)$ (not standard)], $p in [0,1]$, $ZZ_2$, $p$, $p (1 - p)$,
)
We have yet to discuss the simplest possible non-trivial probability sample set: one with
$abs(Omega) = 2$.  Due to the properties of probability, the only possible probability function over
this sample set is
$ P(k) = cases(
    p &quad arrow.double.l k = 1 \
    1 - p &quad arrow.double.l k = 0
  )
$
We have expressed the argument $k$ as an integer, but of course we can define this distribution for
any cardinality $2$ set by mapping ot to $ZZ_2$.  Often it is more convenient to write this as
$ P(k) = p^k (1 - p)^(1 - k) $
It is trivial to show that the mean of this distribution is $p$ and its variance is $p (1-p)$ by
direct calculation.

The extremely, even maximally simple form of distribution means there is not much to say about
it in and of itself.  However, as we will see, it can be used to construct many other distributions.
In this note we will write random variables in this distribution $X tilde "Bern"(p)$, though we
should emphasize that this is not a standard notation.  Sometimes the Bernoulli distribution is
written $B(p)$, but this risks confusion with the binomial or beta distributions.


== Binomial Distribution
#disttable(
  $"Binom"(n,p)$, $n in ZZ_gt.eq, p in [0,1]$, $ZZ_gt.eq$, $n p$, $n p (1 - p)$,
)
Given a set of variables $X_j tilde "Bern"(p)$, the distribution of the sum
$ X = sum_(j=1)^n X_j tilde "Binom"(n,p) $
is distributed according to the binomial distribution, given by
$ P(k) = (n!)/(k! (n-k)!) p^k (1 - p)^(n - k) $
The coefficient $(n!)/(k! (n-k)!)$ is known as the binomial coefficient, often written $vec(n, k)$.
More prosaically, the binomial distribution is the distribution of the number of "successes"
(results of $1$) in $n$ samples of the Bernoulli distribution with probability $p$ (which can also
be thought of as "weighted coin flips").  Since $X$ is expressible as a sum over i.i.d. variables,
in accordance with the CLT, it approaches as Gaussian with mean $n p$ and variance $n p (1 - p)$ for
large $n$, meaning that the binomial distribution approaches the Gaussian distribution for large
$n$.


== Poisson Distribution
#disttable(
  $"Pois"(lambda)$, $lambda > 0$, $ZZ_gt.eq$, $lambda$, $lambda$,
)
The Poisson distribution is that of numbers of events which occur with a "fixed probability per unit
time", a notion we will make more precise in the following.

Suppose that we sample a Bernoulli distribution once every time interval of duration $delta t$.  Let
$Delta$ be a fixed time duration, and let $n thin delta t = Delta$.  We can take the limit $delta t
-> 0$, or equivalently $n -> oo$ while $Delta$ is held fixed.  Then let $p = lambda/n$ for each
Bernoulli distribution each $delta t$ interval.  As we have seen, this is merely a binomial
distribution with $p = lambda / n$, so that the probability function is
$ P(k) &= lim_(n -> oo)
  (n(n-1)(n-2) dots.c (n - k + 1))/(k!) (lambda/n)^k (1 - lambda/n)^(n - k) \
  &= lim_(n -> oo) (n^k + cal(O)(n^(k-1)))/(k!) (lambda/n)^k (1 - lambda/n)^(n - k) \
  &= lim_(n -> oo) lambda^k/(k!) (1 - lambda/n)^(n - k)
$
In the first line we have simply written the binomial coefficient $(n!)/(k! (n - k)!)$ in a more
suggestive way.  Using the identity $lim_(n -> oo) (1 - lambda/n)^k = e^(-lambda)$ we find a simple
expression for the Poisson distribution
$ P(k) = (lambda^k e^(-lambda))/(k!) $
This makes more rigorous our whimsical notion of "fixed probability per unit time".  We could have
written the derivation above in terms of $(lambda/Delta) delta t$, taking the limit $delta t -> 0$
(equivalent to $n -> oo$).  In this sense $lambda/Delta$ can be interpreted as a probability per
unit time.  Note that the parameter $lambda > 0$ is not bounded above, as we may have many
occurrences of an event in time $Delta$.

We might expect that, since we have derived it by taking the $n -> oo$ limit of the binomial
distribution, that the Poisson distribution is merely the Gaussian distribution.  The reason this is
not the case is essentially that the domain of the Poisson distribution is $ZZ_gt.eq$, not $RR$ as
for the Gaussian (i.e. not only integer but also _positive_).  Indeed, the CLT manifests itself in
that the Poisson distribution approaches the Guassian distribution for large $lambda$.
