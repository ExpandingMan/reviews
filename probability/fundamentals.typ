#import "lib.typ": *


= Fundamentals

== The Probability Function
Probability is defined over a set called the *sample set* (sometimes referred to as a *sample
space*).  The elements of this set can be interpreted as "events" or "possibilities".  For example,
the sample set of flipping a coin is ${"heads", "tails"}$; for rolling a cubical die it's $ZZ_6$;
for `2d20` it's $ZZ_20 times ZZ_20$.  For a set to be a sample set
- The elements should be _mutually exclusive_ in some sense.
- _All possible outcomes_ should be contained in the set.

We define the *probability function* over a sample set $Omega$

$ P : 2^Omega -> [0, 1] $

$2^Omega$ is a somewhat fanciful notation for the power set of $Omega$ (i.e. $2^Omega = {A | A
subset.eq Omega})$.#footnote[This notation comes from the fact that $abs(2^Omega) = 2^abs(Omega)$,
that is $2^Omega$ has $2^abs(Omega)$ elements.] $[0,1]$ is the interval on $RR$ from $0$ to $1$,
inclusive of the endpoints.

To "*sample*" $Omega$ means to select an element from it stochastically, such that the probability
to select $omega in Omega$ is $P({omega})$.  This often conincides with some intuitive notion of
sampling, such as the result of some physical process, such as fliping a coin, though we emphasize
that probability is an abstract notion and is not contingent on such a process.

A probability function comes with the following constraints.  For $A, B subset.eq Omega$
$ P(emptyset) = 0 \
  P(Omega) = 1 \
  A sect B = emptyset quad ==> quad P(A union B) = P(A) + P(B)
$<prob-func-props>
The first of these can be interpreted as the statement that sampling $Omega$ results in some $omega
in Omega$.  The second condition is a normalization condition for $P$.

The last property in @prob-func-props states that we can add the probabilities of disjoint sets.
This lets us derive some further properties.  In the remainder of this section, let $A,B subset.eq
Omega$.  From the axioms of set theory

$ A = (A backslash B) union (A sect B) $

Combining this with the third line of @prob-func-props we find

$ P(A) = P(A backslash B) + P(A sect B) $

From this and the positive definiteness of $P$, it immediately follows that

$ P(A) >= P(A sect B) $

Also, since $A union B = (A backslash B) union B$ we have

$ P(A union B) = P(A) + P(B) - P(A sect B) $

Note that this implies a "triangle inequality"

$ P(A) + P(B) >= P(A union B) $

It is sometimes said that "the probability of $A$ or $B$ is $P(A) + P(B)$", here we have seen that
this is only true of disjoint sets (in particular singleton sets of distinct elements, which is the
statement this usually refers to).

Sometimes we will refer to the sample set as a *statistical ensemble*.  This is a term common in
physics.  For example the $6N$-dimensional phase space of $N$ particles in a box can also be thought
of as a statistical ensemble, where the probability $P$ gives the probability of each point in the
phase space.  The foundational assumption of classical statistical mechanics is that all such states
are equally likely.  There are of course uncountably many members of this ensemble, so we must
regularize the probabilities in some way.  We will revisit this concept in a future section.


== Probability Density
So far we have defined a function with the domain $2^Omega$, but it turns out to be very useful to
define some function that has as its domain $Omega$ itself.  We achieve this by "taking the derivative
of $P$"

$ P(A) = integral_A dif x med p(x) $<prob-density>

where $p(x)$ is called the *probability density*.  This can sometimes also be referred to as a
*probability distribution*, but we should caution the reader that this term can refer to any means
of specifying a probability function $P$, directly or indirectly, including the probability density
and explicit enumeration.  In writing @prob-density we have implied more structure in the sample set
$Omega$ than we have previously.  For the time being will interpret @prob-density as implying the
existence of a coordinate system $x : Omega -> RR^n$.  This is _not_ a required feature of a sample
set, and we will often discuss discrete sample sets for which such a description is awkward, but it
is of course a prominent use case.

The properties of integration fit nicely with the axioms of $P$ we have given in @prob-func-props.

$ P(Omega) = integral_Omega dif x med p(x) = 1 \
  p(x) >= 0 #h(4em) forall x in Omega
$<prob-density-props>

We are being somewhat careless in conflating the coordinate $x$ with elements of $Omega$ by writing $x
in Omega$, but we will continue to do so where there is no risk of confusion.  $P(emptyset)=0$ of
course follows from the definition of integration.

We usually consider the probabilty density to be a function $p : Omega -> RR$, however, this is not
a necessary condition for a probability density.  More generally, these are objects on which we can
define an integral over a subset $A subset.eq Omega$, but not necessarily ones that have a
well-defined value for each $x in Omega$. The most ubiquitous example of a density which is not a
function is the Dirac $delta$ function, defined by

$ integral_A dif x thin f(x) thin delta(x - x_0) = cases(
    f(x_0) &quad arrow.l.double x_0 in A,
    0 &quad arrow.l.double x_0 in.not A
  )
$

$delta(x)$ is not a function, but it is a valid probability density.  We will call objects like
these *distributions*.  Instead of giving a rigorous definition, we will suffice it to say that
these are objects over which we can make some consistent definition of integrals on
$Omega$.#footnote[Again, we will frequently conflate $Omega$ with its coordinates without further
apology.]

The introduction of the probability density allows us to describe the probability of samples in an
uncountably large ensemble.  Returning to the example of classical particles with a $6N$-dimensional
phase space, the statement that each member of this ensemble is equally likely is equivalent to the
statement that the probability distribution is $p(z) = "const"$, where $z$ is a point in the phase
space.  The properties of probability then demand $integral_cal(V) dif^(6N)z med p(z) = 1$, where
$cal(V)$ is the entire phase space.


== Cumulative Probability
An important special case is when the coordinate is one dimensional $x : Omega -> RR$, in which we refer
to the probability as *univariate*.  In such cases we define the *cumulative distribution function*
which is the anti-derivative of $p$

$ F(x) := P(xi <= x) = integral_(-oo)^x dif xi med p(xi) $

#footnote[The use of the symbol $F$ here may seem capricious, but it is commonly used.  Often a
probability density $p$ is written $f$.]  From this it follows that

$ P(a <= x <= b) = F(b) - F(a) $

There's not much to say about the cumulative probability in and of itself, but it is often a useful
notational tool.


== Multivariate Probability
The general case where the sample set admits coordinates $x : Omega -> RR^n$ with $n > 1$ is called
*multivariate* probability.  The definition of the probability density in this case is a
straightforward extrapolation of @prob-density.  For example, in $RR^2$

$ P(A) = integral_A dif x dif y med p(x,y) $

A new feature relative to the 1-dimensional case is that we can now define a *marginal* probability
density

$ p_X (x) = integral_(-oo)^oo dif y med p(x,y) $

For now the subscript $X$ can simply be thought of as indicating that this marginal density is a
function of the coordinate component $x$, later we will introduce concepts that make this notation
seem less arbitrary.  We could of course define $p_Y (y)$ analogously by integrating out $x$.

Note that $integral dif x med p_X (x) = 1$ and $p_X (x) >= 0, forall x$ follow trivially from
@prob-density-props.  This means that $p_(X)(x)$ can itself be considered a probability density, and
that the distinction between marginal and "non-marginal" probability densities is merely a
convention.


== Conditional Probability
What is the probability that $x in A$ on the condition that $x in B$?  The preceding discussion does
not make it entirely clear what this means.  To address this, we must define *conditional
probability* $P(A|B)$ (read "the probability of $A$ such that $B$).  Let's impose some requirements
on this definition.  First, we'd like

$ P(A|B) prop P(A sect B) #h(4em) forall A, B subset.eq Omega $

i.e. that the conditional probability is always proportional to the probabilty over $Omega$.  Next,
we require

$ P(B|B) = 1 $

to coincide with our intuitive notion of conditioning (i.e. we must always have $x in B$ if we
impose $x in B$ as a constraint).  We therefore define

$ P(A|B) := P(A sect B)/P(B) $<cond-func>

Likewise, for the density we can define

$ p_(X|Y)(x|y) = (p_(X Y)(x,y))/(p_Y (y)) $<cond-density>

@cond-func and @cond-density look the same, but do not mean exactly the same thing.  They are
related by

$ P(x in A | y = y_0) = integral_A dif x med p_(X|Y)(x | y=y_0) $

We will deliberately gloss over what happens if $p_Y (y) = 0$; either there is some consistent way
to regularize @cond-density or we cannot define it.

The way we have presented this, it is entirely explicit that $P(A|B)$ and $P(A sect B)$ are two
completely different things, but it's worth pointing out that they often get confused when the
context is less obvious.  In particular, if $P(B)$ is very small then by @prob-func-props so must be
$P(A sect B)$, but this does _not_ necessarily imply that $P(A|B)$ is small.

== Random Variables
When dealing with multivariate probability densities, it is often convenient to introduce the
concept of *random variables*.  A random variable is a map $X : Omega -> cal(M)$ where $cal(M)$ is
an $n$-dimensional differntiable manifold (which we will usually take to be $RR^n$).  Random
variables are mostly a notational convenience that make it easier to talk about more complex
relationships between elements of a sample set, and they aren't really any different from the
coordinates on $Omega$ we've already been discussing.  For example, let's define $f : RR^2 -> RR$
and write

$ Z = f(X,Y) $

where $X,Y,Z$ are random variables.  By definition, the probability distribution of $Z$ is

$ p_Z (z) = integral dif x dif y med delta(z - f(x,y)) med p_(X Y) (x,y) $

We will sometimes use the notation $X tilde p$ to mean that $p$ is the probability density for the
random variable $x$, read "$X$ is distributed according to $p$".

We say that two random variables are *independent* if their distribution can be factorized

$ p_(X Y) (x, y) = p_X (x) med p_Y (y) $<independent-vars>

Note that in this case $p_(X|Y)(x|y) = p_(X)(x)$, confirming that our definitions are consistent
with the intuitive notion that the conditional probability density of indepndent variables is the
same as the corresponding marginal probability density.  The normalization conditions ensure that
@independent-vars is the unique factorization of $p_(X Y)$ (i.e. the factors are always the marginal
densities).

Two random variables are *independent and identically distributed* (abbreviated *i.i.d.*) if in
addition to @independent-vars $p_X (xi) = p_Y (xi)$.

If we write an expression that relates multiple random variables, it must be possible to define them
over the same sample set so that they share an overall distribution called the *joint distribution*.
For example, if we define two random variables $X,Y$, for any function $f(X,Y)$ to make sense, it
must be possible to define $p_(X Y)(x,y)$.  If $X$ and $Y$ are independent, then, by definition
$p_(X Y)(x,y) = p_(X)(x) thin p_(Y)(y)$, while in the general case the relationship is
more complicated, but regardless, we must be able to define $p_(X Y)(x,y)$ in order to define the
distribution of $f(X,Y)$.


=== Example: Sum of Two Independent Random Variables
Consider $Z = X + Y$ where $X$ and $Y$ are independent.  According to our definition
$ p_Z (z) = integral dif x dif y thin delta(z - x - y) thin p_X (x) thin p_Y (y) $
Integrating over one of the variables we have
$ p_Z (z) = integral dif x thin p_Y (z - x) thin p_X (x) $<sum-rand-vars>
Note that we are treating it as implicit that $p_Z (z) = 0$ for any $z$ which is not expressible as
$z = x+y$, which is important if, for example, $x$ or $y$ have a finite range.

@sum-rand-vars states that the distribution of the sum of two random variables is the convolution of
their distributions.  We will repeatedly find this important when discussing prominent examples of
probability densities.

== Probability on Differentiable Manifolds
As we have seen, we can define random variables as maps to any differentiable manifold#footnote[For
those unfamiliar with manifolds and differential geometry, most of this section can be understood by
replacing the term "differentiable manifold" with "$RR^n$", which is a specific example of such a
manifold.], and @prob-density furnishes a definition of probability on such manifolds.  For the
definition of probability on a manifold to be self-consistent, it should not depend on choice of
coordinates. That is

$ integral_A dif x' thin p'(x') = integral_A dif x thin p(x) #h(4em) forall A in 2^cal(M) $

where $x'$ are new coordinates and $p'$ is the same probability density in the new coordinate
system.  Since $A$ is arbitrary, we must have

$ p'(x') thin dif x' = p(x) thin dif x $<density-change-coords>

or

$ p'(x') = p(x) abs((diff x)/(diff x')) $

where we express $x$ as a function of $x'$ and $abs((diff x)/(diff x'))$ is the Jacobian of the
transformation.

=== Probability as a Measure
A measure is simply a positivie-definite function $mu : 2^Omega -> RR$ on the superset of a set
$Omega$ such that $mu(nothing) = 0$ and $(A subset.eq Omega)(B subset.eq Omega)[A sect B =
nothing] ==> mu(A union B) = mu(A) + mu(B)$.  This is entirely redundant with all our previous
discussion of probability which is clearly a "normalized" measure, in the sense that we require
$P(Omega)=1$.  A measure in turn allows us to define integration by, roughly speaking, taking the
sum of an arbitrary partition in sum region of $Omega$.  All this is to make the trivial statement

$ P(A) = integral_A dif P(omega) $

This is mostly a transparent re-telling of our previous statements on probability and probability
density functions, but note that in this formulation we are not necessarily assuming the existence
of a coordinate map on $Omega$.  This can provide us with a more convenient way of discussing
integrals of random variables.  For example

$ expect(X) = integral_Omega dif P med X $

This is often much more convenient than writing some integral over $p_(X)$ for which we must
introduce coordinates.  As such, we may often make use of this notation in the future.


=== Probability as a Differential Form
This section requires some knowledge of the exterior calculus and the language of differential
forms.

We can recast @prob-density on an $n$-dimensional differnetiable manifold $cal(M)$ as an integral
$ P(A) = integral_A dif P $
Since $A subset.eq cal(M)$ is arbitrary, $dif P$ must be an $n$-form, which on an $n$-dimensional
manifold is unique up to an overall factor.  All we have done here is to re-define the probability
density $p(x)$ as the coefficient of this $n$-form
$ dif P = p(x_1,x_2,...,x_n) thin (dif x_1 and dif x_2 and dots.c and dif x_n) $

Clearly @density-change-coords now follows from the standard transformation properties of the basis
$n$-forms.  On manifolds equipped with a metrix, we must include a geometric factor in explicit
coordinate representations of the integration over $p(x)$
$ P(A) = integral_A dif^n x thin sqrt(plus.minus g) thin p(x) $

At our level of rigor, differential forms are essentially measures#footnote[Technically there is an
important distinction involving orientation which is well beyond the scope of this review.], so this
is consistent with our discussion in the last section.


=== Example: Rotations
Consider the transformation
$ x arrow.bar R(phi) thin x $
where $x$ is a vector and $R(phi)$ is a rotation matrix.  Such matrices form a group, specifically
$"SO"(N)$.  One of the properties of a group is that every element must have an inverse, in our caes
$R^(-1) (phi) R(phi) = 1$.  For $x' = R(phi) thin x$ we therefore have
$ p'(x') = p(x) abs((diff x)/(diff x')) = p(R^(-1) (phi) x') $
since $abs(R^(-1)) = 1$ is a property of $"SO"(N)$.


== Bayes' Lemma
We now come to a very famous but trivial result.  By combining @cond-func for $P(A|B)$ and $P(B|A)$,
we find

$ P(A|B) = (P(B|A) P(A))/P(B) $<bayes-lemma>

We can repeat the same derivation using our expression for conditional density to obtain
$ p_(X|Y)(x|y) = (p_(Y|X)(y|x) p_(X)(x))/(p_(Y)(y)) $

The reason this result is so famous is that it is very common for the probabilities of some set not
to be directly observable.  That is, you may want to know $P(A|B)$, but can only observe $P(B|A)$.
@bayes-lemma relates these.

Note that

$ p(x) = integral dif y thin p(x|y) thin p(y) $

where we have dropped the subscripts of the densities $p$ for convenience.  This is true for all
random variables, not only independent ones, as it is simply a consequence of the definition of
$p(x|y)$.

The factor $P(A)$ in the numerator of the right hand side of @bayes-lemma is commonly referred to as
a *prior*.  As we will discuss, in many applications it is not directly observable and must be
postulated.  The denominator $P(B)$ can be thought of as a normalization factor, and is often
determined by requiring $P(Omega)=1$.

== Epistemology
Most of the above discussion has been rather abstract, but to apply probability to the real world,
we must resort to some discussion of interpretation.  We caution the reader that interpretation of
probability theory can be considered an enormous subject in and of itself, in both the realms of
pure mathematics and philosophy, but here our goal is to avoid the proverbial rabbit hole and
provide the bare minimal context required for a useful working knowledge of probability.

There are primarily two equivalent interpretations of probability theory.  As a common premise,
imagine that we have prepared some number $n$ of _identical_ experiments.  The word identical is
doing a lot of work here, but for now we decline a more detailed discussion of what it means. For
our purposes it will suffice to assume we have formulated some reasonable definition.  The $i$th
experiment concludes with an observation $omega_i in Omega$, where $Omega$ is the sample space and
the set of all possible outcomes of the experiment.

The *frequentist* interpretation asserts

$ P(A) = lim_(n -> oo) 1/n sum_(i=1)^n I(omega_i in A) #h(4em) forall A subset.eq Omega $

where $I$ is an indicator that gives $1$ if its argument is true and $0$ otherwise.  Thus, in this
interpretation, the probability of $A$ is simply the ratio of events in $A$ to the total number of
events as that number approaches infinity.  Of course, the statement $P(A) = a_0$ does not
necessarily mean that it has been observed infinitely many times, but that we could reject
$P(A)=a_0$ as a plausible statement if repeated observations fail to converge to this value.

The other prominent interpretation of probability theory is called the *Bayesian* interpretation. It
is easier to describe it by imagining that we have some theory the parameters of which we will
collectively refer to as $theta$.  Bayesian probability treats $theta$ as a collection of random
variables.  Then, by definition

$ P(theta|(omega_1,omega_2,...,omega_n)) =
  (P((omega_1,omega_2,...,omega_n)|theta) P(theta))/(P((omega_1,omega_2,...,omega_n)))
$

where $(omega_1,omega_2,...,omega_n)$ is a sequence of observations.  Unobservability of the
denominator seems like a problem, but as we have discussed it can be thought of as a normalization
factor. $P((omega_1,...,omega_n)|theta)$ should be possible to compute from the theory and can be
tought of as a requisite for a valid theory.  The other factor in the numerator, the prior
$P(theta)$, poses a more serious and fundamental challenge.  One way to proceed would be to choose
it as a postulate, but in so doing we risk choosing a "pathological" prior, for example a delta
function.  Such pathological priors could prevent $P(theta|(omega_1, ..., omega_n))$ from
converging to an empirically plausible result.

The relative merits of each of these two interpretations are still the subject of much debate.  In
my opinion, the frequentist interpretation is simpler and comes with significantly less "conceptual
baggage", but these interpretations are nevertheless equivalent.  Furthermore, the procedure implied
by the Bayesian interpretation, which that interpretation treats as fundamental (i.e. using the
axioms of probability theory to update assumptions with new evidence) are of great practical
importance even if one takes the frequentist view entirely literally.  It is useful to maintain a
pluralistic mindset in regard to this subject, as it is for many others.
