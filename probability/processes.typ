#import "lib.typ": *

= Stochastic Processes
A stochastic processes is an uncountable set of random variables parameterized by some parameter
$t$.  For example ${X(t) | forall t in RR^n}$ where each $X(t)$ is a random variable.  That is, we can
think of $X$ as $X(t;omega)$ where $omega in Omega$ for a sample space $Omega$ such that $X(t; dot)
: Omega -> cal(M)$ is a random variable for each $t in RR^n$.  Another way to think about this is as a
"function-valued random variable".  Obviously, this is a very broad class of objects samples of
which can include both smooth and discontinuous functions.  To characterize them, we must consider
the probability density of each $X(t)$.  For notational convenience we define

$ m_(X)(t) = expect(X(t)) $

In general, the probability densities at different points are related: that is $X(t_1)$ and $X(t_2)$
can be correlated.  Therefore, the covariance

$ K_(X)(t_1, t_2) = cov[X(t_1), X(t_2)] $
 
plays a key role in the study of stochastic processes.  We refer to $K_X$ as the _covariance
function_ for the random process $X$.  In the future we will drop the subscript $X$ where convenient
and where there is no risk of confusion.

A process is called _strictly stationary_ if its distribution does not depend on $t$.  More
precisely, this means that the joint distribution of ${X(t) | t in A}$ is equal to the joint
distribution of ${X(t) | t in B}$ $forall A,B in RR^n$.  A weaker notion of stationarity, sometimes
called _wide stationarity_ requires
that
$ m(t) = "const"  wide and wide
  K(t_1,t_2) = K(g(t_1, t_2))
$
where $g : RR^n times RR^n -> RR$ is a metric.  Of course, $K$ can also possess additional symmetries, such as
rotation, and we can classify Gaussian processes by the symmetry properties of $K$.


== Gaussian Processes
An important special case of a stochastic process is a _Gaussian process_ in which $X(t) tilde
cal(N)_(n)(m(t), K(t,t))$ $forall t$.  In this case, $m(t)$ and $K(t_1,t_2)$ fully specify the
distribution of the process $X$ because these are the only parameters of the distribution.

The properties of multivariate Gaussians that we discussed in @sec-multivar-gaussian now tell us
everything we need to know about the Gaussian process $X$.  As an important example of this, suppose
we have observed $X(t)$ at some set of parameters $t_1,t_2,...,t_l$.  To compute the conditional
distribution $X(t) | X(t_1), X(t_2), dots.c, X(t_l)$ we define
$ Sigma = mat(K(t,t), Sigma^(*)(t); Sigma^(*upright(T))(t), Sigma^(**)) \
  Sigma^*_j = K(t,t_j) #h(6em)
  Sigma^(**)_(i j) = K(t_i, t_j) #h(6em) (1 <= i <= l)(1 <= j <= l)
$
These can be simply plugged into @conditional-multivar-gaussian to obtain the conditional
distribution explicitly.

Strict stationarity and wide stationarity, discussed above, are equivalent for Gaussian processes.

//TODO: would be nice for this section to be more complete
