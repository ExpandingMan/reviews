
function help()
    printstyled("Review website builder.\n", color=:blue)
    printstyled("""
                - `quartorender()`: generate the site
                - `build()`:  do all

                """, color=:yellow)
end

const THIS_DIR = dirname(@__FILE__)

function listreviewdirs(dir::AbstractString=THIS_DIR)
    filt = s -> isdir(s) && !startswith(basename(s), ".") && (basename(s) ≠ "site")
    cd(() -> filter(filt, readdir(dir)), dir)
end

quartorender(::Type{Cmd}, dir::AbstractString=THIS_DIR) = Cmd(`quarto render site`; dir)
function quartorender(dir::AbstractString=THIS_DIR) 
    @info("rendering website...")
    run(quartorender(Cmd, dir))
end

function copyaux(dir::AbstractString, rev::AbstractString) 
    src = joinpath(dir,rev)
    dst = joinpath(dir,"site","_site",rev)
    ispath(dst) || mkpath(dst)
    for f ∈ readdir(src)
        if endswith(f, r"\.pdf|\.png|\.jpg|images")
            (a, b) = joinpath.((src, dst), (f,))
            @info("copying $rev/$f...")
            cp(a, b, force=true)
        end
    end
end

function copyauxs(dir::AbstractString, revs=listreviewdirs(dir))
    @info("copying PDF's and images...")
    foreach(r -> copyaux(dir, r), revs)
end

function build(dir::AbstractString=THIS_DIR, revs=listreviewdirs(dir))
    quartorender(dir)
    copyauxs(dir, revs)
    @info("done!")
end


if abspath(PROGRAM_FILE) == @__FILE__
    isinteractive() ? help() : build()
end
